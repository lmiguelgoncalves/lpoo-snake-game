package snake.user;
import snake.logic.*;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import snake.logic.SnakeApplet;

public class MenuCanvas extends Canvas implements Runnable, KeyListener {

	private final int BOX_HEIGHT = 20, BOX_WIDTH = 20;
	private int choice1 = 1, choice2;
	private Boolean game = false;
	private int chosenMenu = 1;

	BufferedImage mainMenu1, mainMenu2;
	BufferedImage secondMenu1, secondMenu2;
	BufferedImage youLoose, youWin;
	Image menuImage;

	private Thread runThread;

	/*
	public void setImages() {
		try {                
			mainMenu1 = ImageIO.read(new File("MainMenu1.png"));
		} catch (IOException ex) {
		}
		try {                
			mainMenu2 = ImageIO.read(new File("MainMenu2.png"));
		} catch (IOException ex) {
		}
	}*/


	public void paint(Graphics g) {

		if (runThread == null) {
			this.setPreferredSize(new Dimension(640, 480));
			this.addKeyListener(this);
			runThread = new Thread(this);
			runThread.start();
		}
		if (chosenMenu == 1)  {
			drawMainMenu(g);
		} else if (chosenMenu == 2) {
			drawSecondMenu(g);
		}

	}

	public void drawMainMenu(Graphics g) {
		if (choice1 == 1) {
			URL imagePath = MenuCanvas.class.getResource("MainMenu1.png");
			menuImage = Toolkit.getDefaultToolkit().getImage(imagePath);
		} else if (choice1 == 2) {
			URL imagePath = MenuCanvas.class.getResource("MainMenu2.png");
			menuImage = Toolkit.getDefaultToolkit().getImage(imagePath);
		}

		g.drawImage(menuImage,0,0,640,480,this);
	}

	public void drawSecondMenu(Graphics  g) {

	}


	@Override
	public void keyPressed(KeyEvent e) {

	}


	@Override
	public void keyReleased(KeyEvent e) {
		if (chosenMenu == 1) {
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				choice1 = 1;
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				choice1 = 2;
			} else if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) {
				if (choice1 == 1) {
					choice1 = 1;
					SnakeApplet.visibleCanvas = 1;
					choice1 = 1;
					SnakeApplet.switchMenus();
				} else if (choice1 == 2) {
					System.exit(0);
				}
			}
		} else if (chosenMenu == 2) {
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				choice2 = (choice2 - 1) % 3 ;
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				choice2 = (choice2 + 1) % 3 ;
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}


	@Override
	public void run() {
		repaint();
		while (!game) {
			repaint();

			try {
				Thread.currentThread();
				Thread.sleep(100);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
