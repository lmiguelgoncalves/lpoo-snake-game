package snake.test;

import static org.junit.Assert.*;

import java.awt.Point;
import java.util.List;

import org.junit.Test;

import snake.logic.Direction;
import snake.logic.Snake;
import snake.logic.SnakeCanvas;

public class snakeTest {
	/**
	 * Tests if snake constructor is correct
	 * Snake should start with head at (0,2)
 	*/
	@Test
	public void testInitialSnake() {
		Snake snake = new Snake();
		assertTrue(snake.getScore() == 0 && snake.isSnakeAlive());
		assertEquals(2, snake.getHead().y);
		assertEquals(0, snake.getHead().x);
	}
	
	/**
	 * Tests if snake moved one space forward
	 * initial snakes's head coords (0,2) -> final (0,3) 
	 */
	@Test
	public void testMove() {
		Snake snake = new Snake();
		move(snake);
		assertEquals(3, snake.peekFirst().y);
		assertEquals(0, snake.peekFirst().x);
	}
	
	/**
	 * Tests if snake ate fruit and grew
	 * Initial snake's size is 3 and becomes 4
	 */
	@Test
	public void testGetFruit() {
		Snake snake = new Snake();
		move(snake);
		move(snake);
		assertTrue(snake.getSize() == 4);
	}

	/**
	 * Overload of original move function from SnakeCanvas
	 * @param snake
	 */
	public void move(Snake snake) {
		Point food = new Point(0, 4);
		Point newPoint = new Point();
		newPoint = new Point(snake.getHead().x, snake.getHead().y + 1);
		snake.remove(snake.peekLast());
		if (newPoint.equals(food)) {
			//snake eats food
			Point addPoint = newPoint;
			snake.push(addPoint);
			newPoint = new Point(snake.peekLast().x, snake.peekLast().y + 1);
		} else if (newPoint.x < 0 || newPoint.x > SnakeCanvas.GRID_WIDTH -1 || newPoint.y < 0 || newPoint.y > SnakeCanvas.GRID_HEIGHT -1) {
			//out of bounds
			return;
		} else if (snake.contains(newPoint)) {
			//snake collides with itself
			return;
		}
		//if we reach this point game continues
		snake.push(newPoint);
	}

}
