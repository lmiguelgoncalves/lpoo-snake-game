package snake.logic;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class SnakeCanvas extends Canvas implements Runnable, KeyListener {

	public final int BOX_HEIGHT = 20, BOX_WIDTH = 20;

	public static final int GRID_HEIGHT = 20;
	public static final int GRID_WIDTH = 30;

	Snake snake;
	private Point food;
	private int direction = Direction.NO_DIRECTION;
	private Boolean mainMenu = false, secondMenu = false, won = false;
	Boolean gameEnd = false;


	private Thread runThread;	


	public void paint(Graphics g) {

		if (runThread == null) {
			this.setPreferredSize(new Dimension(640, 480));
			this.addKeyListener(this);
			runThread = new Thread(this);
			runThread.start();
		}
		if (snake == null) {
			generateDefaultSnake();
			placeFruit();
		}

		drawWallsOnly(g);
		drawFood(g);
		drawSnake(g);
		drawScore(g);

	}

	public void update (Graphics g) {

		Graphics offScreenGraphics;
		BufferedImage offScreenImage = null;
		Dimension d = this.getSize();

		offScreenImage = new BufferedImage(d.width, d.height,BufferedImage.TYPE_INT_ARGB);
		offScreenGraphics = offScreenImage.getGraphics();
		offScreenGraphics.setColor(Color.black);
		offScreenGraphics.fillRect(0, 0, d.width, d.height);
		offScreenGraphics.setColor(Color.black);
		paint(offScreenGraphics);

		//flip
		g.drawImage(offScreenImage, 0, 0, this);
	}

	public void generateDefaultSnake() {
		snake = new Snake();
		direction = Direction.NO_DIRECTION;
	}

	public void move() {
		Point head = snake.peekFirst();
		Point newPoint = head;
		switch (direction) {
		case Direction.NORTH:
			newPoint = new Point(head.x, head.y - 1);
			break;

		case Direction.SOUTH:
			newPoint = new Point(head.x, head.y + 1);
			break;

		case Direction.WEST:
			newPoint = new Point(head.x - 1, head.y);
			break;

		case Direction.EAST:
			newPoint = new Point(head.x + 1, head.y);
			break;
		}

		//removes snake's tail
		if (this.direction != Direction.NO_DIRECTION) {
			snake.remove(snake.peekLast());
		}
		if (food != null) {

			if (newPoint.equals(food)) {
				//snake eats food
				Point addPoint = (Point) newPoint.clone();
				switch (direction) {
				case Direction.NORTH:
					newPoint = new Point(head.x, head.y - 1);
					break;

				case Direction.SOUTH:
					newPoint = new Point(head.x, head.y + 1);
					break;

				case Direction.WEST:
					newPoint = new Point(head.x - 1, head.y);
					break;

				case Direction.EAST:
					newPoint = new Point(head.x + 1, head.y);
					break;
				}
				snake.push(addPoint);
				placeFruit();

			} else if (newPoint.x < 0 || newPoint.x > GRID_WIDTH -1 || newPoint.y < 0 || newPoint.y > GRID_HEIGHT -1) {
				//out of bounds
				//checkScore();
				generateDefaultSnake();
				return;
			} else if (snake.contains(newPoint)) {
				//snake collides with itself
				//checkScore();
				generateDefaultSnake();
				return;
			} else if (snake.size() == (GRID_HEIGHT*GRID_WIDTH)) {
				//User Wins

			}
		}

		//if we reach this point game continues
		snake.push(newPoint);

	}

	public void drawGrid(Graphics g) {
		//Draw the outside frame for the game
		g.drawRect(0,0, GRID_WIDTH * BOX_WIDTH, GRID_HEIGHT * BOX_HEIGHT);

		//Draw the horizontal Grid lines
		for (int i = BOX_WIDTH; i < GRID_WIDTH * BOX_WIDTH; i += BOX_WIDTH) {
			g.drawLine(i, 0, i, BOX_HEIGHT*GRID_HEIGHT);
		}

		//Draw the Vertical Grid lines
		for (int i = BOX_HEIGHT; i < GRID_HEIGHT*BOX_HEIGHT; i += BOX_HEIGHT) {
			g.drawLine(0, i, GRID_WIDTH*BOX_WIDTH, i);
		}
	}

	public void drawWallsOnly(Graphics g) {
		//Draw the outside frame for the game
		g.setColor(Color.white);
		g.drawRect(0,0, GRID_WIDTH * BOX_WIDTH, GRID_HEIGHT * BOX_HEIGHT);
	}

	public void drawSnake(Graphics g) {
		g.setColor(Color.green);
		Iterator<Point> iterator = snake.iterator();
		while(iterator.hasNext()){
			Point p =iterator.next();
			g.fillRect(p.x * BOX_WIDTH, p.y * BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
		}

		g.setColor(Color.black);
	}

	public void drawFood(Graphics g) {

		g.setColor(Color.red);
		g.fillOval(food.x * BOX_WIDTH, food.y * BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
		g.setColor(Color.black);
	}

	public void drawScore(Graphics g) {
		g.setColor(Color.red);
		g.setFont(new Font("BankGothic", Font.PLAIN, 30));
		g.drawString("Score: " + snake.getScore(), 0, BOX_HEIGHT * GRID_HEIGHT + 30); 
	}



	public void placeFruit() {
		Random rand = new Random();
		int randomX = rand.nextInt(GRID_WIDTH);
		int randomY = rand.nextInt(GRID_HEIGHT);
		Point randomPoint = new Point(randomX, randomY);
		while(snake.contains(randomPoint)) {
			randomX = rand.nextInt(GRID_WIDTH);
			randomY = rand.nextInt(GRID_HEIGHT);
			randomPoint = new Point(randomX, randomY);
		}
		food = randomPoint;
	}

	@Override
	public void run() {
		this.revalidate();
		repaint();
		while(true) {
			if (this.direction != Direction.NO_DIRECTION) {
				move();
			}
			repaint();

			try {
				Thread.currentThread();
				Thread.sleep(80);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			if(direction != Direction.SOUTH){
				direction = Direction.NORTH;
			}
			break;
		case KeyEvent.VK_DOWN:
			if (direction != Direction.NORTH) {
				direction = Direction.SOUTH;
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (direction != Direction.WEST){
				direction = Direction.EAST;
			}
			break;
		case KeyEvent.VK_LEFT:
			if(direction != Direction.EAST) {
				direction = Direction.WEST;
			}
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {


	}
}
