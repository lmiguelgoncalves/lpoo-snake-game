package snake.logic;

import java.awt.Point;
import java.util.LinkedList;

public class Snake extends LinkedList<Point> {

	private Point head;
	private Point tail;
	private int size;
	private Boolean alive;
	
	public Snake() {
		this.add(new Point(0,2));
		this.add(new Point(0,1));
		this.add(new Point(0,0));
		alive = true;
		head = getHead();
		tail = getTail();
		size = getSize();
		
	}
	
	//sets
	public void setHead() {
		head = this.peekFirst();
	}
	public void setTail() {
		tail = this.peekLast();
	}
	public void setSize() {
		size = this.size();
	}
	public void setDeath() {
		alive = false;
	}
	
	//gets
	public Point getTail() {
		return tail;
	}
	public Point getHead() {
		return head;
	}
	public int getSize() {
		return size;
	}
	public int getScore() {
		if (size() == 3) {
			return 0;
		} else return (size() - 3) * 10;
	}
	public Boolean isSnakeAlive() {
		return alive;
	}
	
}
