package snake.logic;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import snake.user.MenuCanvas;

public class SnakeApplet extends Applet implements ActionListener{

	public static final int MAIN_MENU = 0;
	public static final int SECOND_MENU = 1;
	public static final int SNAKE_GAME = 2;
	public static final int HIGHSCORE = 3;
	public static final int ALTERNATE_GAME = 3;
	
	private static SnakeCanvas snakeC;
	private static MenuCanvas menu;
	public static int visibleCanvas = 0;

	public void init() {
		initStandardGame();
		initMainMenu();

		menu.setBackground(Color.black);
		menu.setVisible(true);
		menu.setFocusable(true);
		menu.setPreferredSize(new Dimension(640, 480));
	}

	public void paint(Graphics g) {
		this.setBackground(Color.black);
		this.setSize(new Dimension(640, 480));
	}

	public void initMainMenu() {

		menu = new MenuCanvas();
		this.add(menu);
		this.setVisible(true);
		this.setSize(new Dimension(640, 480));
	}

	public void initStandardGame() {

		snakeC = new SnakeCanvas();
		this.add(snakeC);
		this.setVisible(true);
		this.setSize(new Dimension(640, 480));
	}

	public static void switchMenus() {
		switch (visibleCanvas) {
		case 0:
			menu.setBackground(Color.black);
			menu.setVisible(true);
			menu.setFocusable(true);
			menu.setPreferredSize(new Dimension(640, 480));
			break;

		case 1:
			
			break;

		case 2:
			snakeC.setBackground(Color.black);
			snakeC.setPreferredSize(new Dimension(640, 480));
			snakeC.setVisible(true);
			snakeC.setFocusable(true);
			snakeC.requestFocus();
			snakeC.revalidate();
			break;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
