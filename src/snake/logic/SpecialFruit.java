package snake.logic;

import java.awt.Point;
import java.util.Random;


public class SpecialFruit extends Point {

	int type;
	boolean active;

	public void setActive() {
		if (active) {
			active = false;
		} else {
			active = true;
		}
	}

	public void setType() {
		Random randomGenerator = new Random();
		type = randomGenerator.nextInt(3);
	}
}
